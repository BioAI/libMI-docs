var searchData=
[
  ['saveproject',['SaveProject',['../classlibmi_1_1_slide.html#a8aa84d458024b232bfa16f8fb261fd0b',1,'libmi::Slide']]],
  ['setannocolor',['SetAnnoColor',['../classlibmi_1_1_anno.html#a743bc4ddb0d2c86d01a66b60d87cfd23',1,'libmi::Anno']]],
  ['setannoelliparam',['SetAnnoElliParam',['../classlibmi_1_1_anno.html#af64f87aba9be98bcf07acfdff066985f',1,'libmi::Anno']]],
  ['setannopolyparam',['SetAnnoPolyParam',['../classlibmi_1_1_anno.html#ac35ba4515f9742042ab773de574bfff8',1,'libmi::Anno']]],
  ['setannorectparam',['SetAnnoRectParam',['../classlibmi_1_1_anno.html#a19d5214d03f42b3c505dc432c95ea222',1,'libmi::Anno']]],
  ['setcompresslevel',['SetCompressLevel',['../classlibmi_1_1_slide.html#ac8d02000cd36b36825988864934f4832',1,'libmi::Slide']]],
  ['setcurrentregion',['SetCurrentRegion',['../classlibmi_1_1_slide.html#acd5042a9c2b37180f6dfd61e69f13f4c',1,'libmi::Slide']]],
  ['setgrade',['SetGrade',['../classlibmi_1_1_slide.html#a855a8a7fba6fa1fe17158d59075fbe61',1,'libmi::Slide']]],
  ['setgrades',['SetGrades',['../classlibmi_1_1_slide.html#a1af5458df569d0c84c459400859b390d',1,'libmi::Slide']]],
  ['setpropertyvalue',['SetPropertyValue',['../classlibmi_1_1_slide.html#a45f37b86f45e26a8c694dc6245a516bb',1,'libmi::Slide']]],
  ['setregionsize',['SetRegionSize',['../classlibmi_1_1_slide.html#a871ed739485bddf715c26493c5264182',1,'libmi::Slide']]],
  ['setsecuremode',['SetSecureMode',['../classlibmi_1_1_slide.html#acbf4f61aba510e64289ac49179605d74',1,'libmi::Slide']]],
  ['setsegmcachesize',['SetSegmCacheSize',['../classlibmi_1_1_slide.html#a9f99522d184feb386e952cd255e60d52',1,'libmi::Slide']]]
];
