var searchData=
[
  ['getannos',['GetAnnos',['../classohil_1_1_slide.html#a4d949722b606bbe27f3c44bd6e93e828',1,'ohil::Slide']]],
  ['geterrorstr',['GetErrorStr',['../classohil_1_1_slide.html#a245855c180cbf61f7b2fc31a1dc40fa3',1,'ohil::Slide']]],
  ['getgrade',['GetGrade',['../classohil_1_1_slide.html#ab8b1b87bdc1437db58426a3bd0cbe4b3',1,'ohil::Slide']]],
  ['getgrades',['GetGrades',['../classohil_1_1_slide.html#a9e7a45743f21353e1db6f4578c4248f2',1,'ohil::Slide']]],
  ['getgradetilesize',['GetGradeTileSize',['../classohil_1_1_slide.html#af622f9dc59cf7ce34dceb8ac2da5a022',1,'ohil::Slide']]],
  ['getheight',['GetHeight',['../classohil_1_1_slide.html#a7de5f8ca10e8ac9416766691c2f84b3b',1,'ohil::Slide']]],
  ['getpropertynames',['GetPropertyNames',['../classohil_1_1_slide.html#ae00b993a2d357ea0b431eaec7e99d789',1,'ohil::Slide']]],
  ['getpropertyvalue',['GetPropertyValue',['../classohil_1_1_slide.html#acc7e03ed7cc12b09984785a4b58629cc',1,'ohil::Slide']]],
  ['getsegmpixelsize',['GetSegmPixelSize',['../classohil_1_1_slide.html#ab2bfea003caec09d3e73aaea57be6ebd',1,'ohil::Slide']]],
  ['getsegmtilesize',['GetSegmTileSize',['../classohil_1_1_slide.html#a5552551237e42516e253087f726f6933',1,'ohil::Slide']]],
  ['gettype',['GetType',['../classohil_1_1_slide.html#a83d0ecd0703fcb2f3bc345a00053f367',1,'ohil::Slide::GetType()'],['../classohil_1_1_anno.html#a8315dfdaddf28c907716e9d9bec1dca0',1,'ohil::Anno::getType()']]],
  ['getwidth',['GetWidth',['../classohil_1_1_slide.html#a4abe811f66b7e57874641715e3abf653',1,'ohil::Slide']]]
];
