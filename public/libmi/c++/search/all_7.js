var searchData=
[
  ['saveproject',['SaveProject',['../classohil_1_1_slide.html#a24b2ef55e37efe36a65bd13e132ebcc2',1,'ohil::Slide']]],
  ['setcompresslevel',['SetCompressLevel',['../classohil_1_1_slide.html#a7a458e03b7ae93b8cd6c22ca40e2c754',1,'ohil::Slide']]],
  ['setcurrentregion',['SetCurrentRegion',['../classohil_1_1_slide.html#ac8777ad385684043348354ee11998b17',1,'ohil::Slide']]],
  ['setgrade',['SetGrade',['../classohil_1_1_slide.html#a1cc664385b265e00f880567f8d30d4a6',1,'ohil::Slide']]],
  ['setgrades',['SetGrades',['../classohil_1_1_slide.html#af109e41231a6a2098c8a907e1c6bef36',1,'ohil::Slide']]],
  ['setpropertyvalue',['SetPropertyValue',['../classohil_1_1_slide.html#aabed4386dfab31900b4155229ffcbf81',1,'ohil::Slide']]],
  ['setregionsize',['SetRegionSize',['../classohil_1_1_slide.html#ab3198d72ebe20c01f003f50fe8bb5a7c',1,'ohil::Slide']]],
  ['setsecuremode',['SetSecureMode',['../classohil_1_1_slide.html#af8e059365e3b00ca02793902d5605c5e',1,'ohil::Slide']]],
  ['setsegmcachesize',['SetSegmCacheSize',['../classohil_1_1_slide.html#a068b512f2a1ae2e885be35986d42740f',1,'ohil::Slide']]],
  ['slide',['Slide',['../classohil_1_1_slide.html',1,'ohil::Slide'],['../classohil_1_1_slide.html#a9ee0e90a226dfda90d3a51d97adbe075',1,'ohil::Slide::Slide()']]],
  ['slideprivate',['SlidePrivate',['../classohil_1_1_slide_private.html',1,'ohil']]]
];
